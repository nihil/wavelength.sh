Quick script to dump wavelength info. I use it for measuring antenna length when playing with SDR.

Usage: wavelength.sh 1324.5678 Hz|kHz|MHz|GHz

Example output:

```
$ ./wavelength.sh 433.920 MHz

Wavelength: 433.920MHz (λ = C/f)

λ = 0.6909m
    69.0900cm
    690.9000mm

½ = 0.3454m
    34.5450cm
    345.4500mm

¼ = 0.1727m
    17.2725cm
    172.7250mm
```


