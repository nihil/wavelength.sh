#!/bin/bash
#
# quick script to dump wavelength info
# 
# pubsrc [at] ndnihil.org
#

lambda="λ"    # wavelength in meters
half="½"      # one half
quarter="¼"   # one quarter
C=299792458   # speed of light, meters per second

function usage {
  echo "Usage: `basename $0` 1324.5678 Hz|kHz|MHz|GHz"
  exit 1
}

case $2 in
  [hH][zZ])
    unit="Hz"
    div=1
    ;;
  [kK][hH][zZ])
    unit="kHz"
    div=1000
    ;;
  [mM][hH][zZ])
    unit="MHz"
    div=1000000
    ;;
  [gG][hH][zZ])
    unit="GHz"
    div=1000000000
    ;;
  *)
    usage
    ;;
esac

l=`printf '%1.4f' $(bc -l<<<\(${C}/${1}\)/${div})`
cm=`printf '%1.4f' $(bc -l<<<\(${l}*100\))`
mm=`printf '%1.4f' $(bc -l<<<\(${l}*1000\))`

hl=`printf '%1.4f' $(bc -l<<<\(\(${C}/${1}\)/${div}\)/2)`
hcm=`printf '%1.4f' $(bc -l<<<\(${l}*100\)/2)`
hmm=`printf '%1.4f' $(bc -l<<<\(${l}*1000\)/2)`

ql=`printf '%1.4f' $(bc -l<<<\(\(${C}/${1}\)/${div}\)/4)`
qcm=`printf '%1.4f' $(bc -l<<<\(${l}*100\)/4)`
qmm=`printf '%1.4f' $(bc -l<<<\(${l}*1000\)/4)`


echo ""
echo "Wavelength: ${1}${unit} (${lambda} = C/f)"
echo ""
echo "${lambda} = ${l}m"
echo "    ${cm}cm"
echo "    ${mm}mm"
echo ""
echo "${half} = ${hl}m"
echo "    ${hcm}cm"
echo "    ${hmm}mm"
echo ""
echo "${quarter} = ${ql}m"
echo "    ${qcm}cm"
echo "    ${qmm}mm"
echo ""

#_eof_
